<!DOCTYPE html>
<html lang="es">

<head>
    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>La Criba - Portal administrativo</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">
    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    
    <script src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script src="./js/general.js"></script>

</head>

<body>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.5&appId=428768733870473";
    fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    
    <?php
        session_start();
        $_SESSION["userlogged_error"] = "8";
    ?>
    <div id="wrapper">
        <?php
        include_once("./Procesos/_util.php");
        
        if ( isset($_POST["userlogged_name"]) ) {
            verificar_usuario($_POST["userlogged_name"], $_POST["userlogged_password"]);
         } elseif ( isset($_SESSION["userlogged_name"]) ) {
            include_once("./Vistas/_menu.php");
            
         }
        ?>
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            La Criba
                        </h1>
                    </div>
                </div>

                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-bar-chart-o fa-fw"></i> Portal Administrativo </h3>
                            </div>
                            <div class="panel-body">
                                <!--<div id="morris-area-chart"></div>-->
                                <?php
                                    include_once("./Procesos/_util.php");
                                    if (isset( $_SESSION["userlogged_name"]) ) {
                                        include_once("./Vistas/_bienvenidausuarios.php");
                                    } else {
                                        if($_SESSION["userlogged_error"] == "1")
                                        {
                                            $_SESSION["userlogged_error"] = "8";
                                            include_once("./Vistas/_nouserfound.php");
                                        }
                                        include_once("./Vistas/_iniciarsesion.php");
                                        echo "<br> <br>";
                                        include_once("APIS/_layout.php");
                                    }
                                   
                                    
                                    if( $_POST["agregar_mensualidad_listo"])
                                    {
                                        agregar_mensualidad($_POST["curso"], $_POST["mensualidad"], $_POST["fecha"]);
                                    }
                                     if( $_POST["agregar_mensualidad_nuevo"])
                                    {
                                        agregar_mensualidad($_POST["curso"], $_POST["mensualidad"], $_POST["fecha"]);
                                        include_once("./Vistas/_agregarmensualidad.php");
                                    }
                                    if( $_POST["ingresar_usuario_listo"])
                                    {
                                        ingresar_usuarios($_POST["nombre"], $_POST["direccion"], $_POST["sexo"], $_POST["tel"], $_POST["email"], $_POST["rol"], $_POST["password"]);
                                    }
                                    if( $_POST["agregar_usuario_mas"])
                                    {
                                        ingresar_usuarios($_POST["nombre"], $_POST["direccion"], $_POST["sexo"], $_POST["tel"], $_POST["email"], $_POST["rol"], $_POST["password"]);
                                        include_once("./Vistas/_adduser.php");
                                    }
                                    
                                    if( $_POST["borrar_usuario_listo"])
                                    {
                                        borrar_usuario($_POST["ID"]);
                                    }
                                    if($_POST["buscar_usuario_listo"])
                                    {
                                        mostrar_usuario($_POST["ID"]);
                                    }
                                    
                                    if($_POST["buscar_usuario_update_listo"])
                                    {
                                        updates_usuario($_POST["ID"]);
                                       
                                    }
                                    
                                    if($_POST["daw"])
                                    {
                                        
                                       updates($_POST["ID"], $_POST["nombre"] , $_POST["domicilio"] , $_POST["sexo"] , $_POST["telefono"] , $_POST["email"], $_POST["id_rol"], $_POST["password"]);
                                    }
                                    if( $_POST["update_usuario_listo"])
                                    {
                                        update_usuarios($_POST["ID"], $_POST["nombre"], $_POST["direccion"], $_POST["sexo"], $_POST["tel"], $_POST["email"], $_POST["rol"], $_POST["password"]);
                                    }
                                    
                                    if( $_POST["agregar_beca_listo"])
                                    {
                                        agregar_beca($_POST["ID"], $_POST["beca"], $_POST["curso"], $_POST["fechainicio"], $_POST["fechafin"]);
                                    }
                                    if( $_POST["agregar_beca_mas"])
                                    {
                                        agregar_beca($_POST["ID"], $_POST["beca"], $_POST["curso"], $_POST["fechainicio"], $_POST["fechafin"]);
                                        include_once("./Vistas/_agregarbeca.php");
                                    }
                                     if( $_POST["eliminar_mensualidades"])
                                    {
                                        foreach ($_POST["mensaborrar"] as $x){
                                            eliminar_mensual($x);}
                                    }
                                    if( $_POST["eliminar_beca"])
                                    {
                                        foreach ($_POST["becaborrar"] as $x){
                                            eliminar_beca($x);}
                                    }
                                    if( $_POST["agrega_pago"]) {
                                        agrega_pago ($_POST["id"], $_POST["cantidad"], $_POST["date"], $_POST["tipo_de_pago"]);
                                    }
                                    if( $_POST["pagos_por_mes"]) {
                                        pagos_por_mes ($_POST["month"], $_POST["year"]);
                                    }
                                     if( $_POST["agregar_curso_otro"]) {
                                        agregar_curso ($_POST["nombre"], $_POST["fechainicial"], $_POST["fechafinal"], $_POST["semestre"], $_POST["faltas"], $_POST["estudiantes"]);
                                        include_once("./Vistas/_agregargrupo.php");
                                    }
                                    if( $_POST["eliminar_cursos"])
                                    {
                                        foreach ($_POST["cursosaborrar"] as $x){
                                            eliminar_cursourso;}
                                    }
                                ?><!--mensaborrarcursosaborrar-->
                            </div><!--osmensualcurso-->
                        </div>
                    </div>
                </div>
                <!-- /.row -->


            </div>
            <!-- /.container-fluid -->
    
        </div>
        <!-- /#page-wrapper -->
    
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>

</body>
<footer>
</footer>
</html>
