$(document).ready( function() {
    //help https://www.youtube.com/watch?v=Fw7xu4obzTQ
    //inicial
    //$('#content').load('./Vistas/_bienvenidausuarios.php');
    
    //con menú
    $('ul#mensualidades li a').click(function() {
        var page = $(this).attr('href');
        $('#content').load('./Vistas/'+page+'.php');
        return false;
    });
    
    $('ul#usuarios li a').click(function() {
        var page = $(this).attr('href');
        $('#content').load('./Vistas/'+page+'.php');
        return false;
    });
    
    $('ul#becas li a').click(function() {
        var page = $(this).attr('href');
        $('#content').load('./Vistas/'+page+'.php');
        return false;
    });
    
     $('ul#grupos li a').click(function() {
        var page = $(this).attr('href');
        $('#content').load('./Vistas/'+page+'.php');
        return false;
    });
    
     $('ul#pagos li a').click(function() {
        var page = $(this).attr('href');
        $('#content').load('./Vistas/'+page+'.php');
        return false;
    });
    
});