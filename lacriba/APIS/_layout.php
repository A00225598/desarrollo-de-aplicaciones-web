                <div class="row">
                    <div class="col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-long-arrow-right fa-fw"></i> Encuéntranos</h3>
                            </div>
                            <div class="panel-body">
                                <div id="morris-donut-chart"></div>
                                <div class="text-center">
                                    <?php include_once("APIS/_maps.php"); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-long-arrow-right fa-fw"></i> Facebook</h3>
                            </div>
                            <div class="panel-body">
                                <?php include_once("APIS/_facebook.php"); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-long-arrow-right fa-fw"></i> ¿Qué dicen de nosotros?</h3>
                            </div>
                            <div class="panel-body">
                                <?php include_once("APIS/_twitter.php"); ?>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="panel-body">
                       <?php include_once("APIS/_translate.php"); ?>
                    </div>
                </div>
                <!-- /.row -->