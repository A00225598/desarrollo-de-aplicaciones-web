<!DOCTYPE html>
<html>
  <head>
    <style>
      #map {
        width: auto;
        height: 350px;
      }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script>
      function initialize() {
        var myLatLng = {lat: 20.531405, lng: -100.452488};
        var mapCanvas = document.getElementById('map');
        var mapOptions = {
          center: new google.maps.LatLng(20.531405,-100.452488),
          zoom: 16,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(mapCanvas, mapOptions);
        
        var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: 'Escuela de Arte La Criba'
         });;
    
      }
      
      google.maps.event.addDomListener(window, 'load', initialize);
      
    </script>
  </head>
  <body>
    Teléfono: 195 5760<br>
    Dirección: Retorno Don José #11 <br>
    Parque Industrial Balvanera<br>
    Querétaro<br>
    <br>
    Más información:<br>
    <a href="http://www.lacriba.mx/">La Criba</a><br>
    <div id="map"></div>
  </body>
</html>