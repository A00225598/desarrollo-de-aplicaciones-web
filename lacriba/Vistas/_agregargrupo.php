    <form method="post" action="../index.php">
    <h2>Agregar un curso</h2>
    <p><span class="err">(*) Todos los campos son requeridos.</span></p>
        
    <div class="form-group input-group" style="width:20%" >
        <label>Nombre del curso</label>
        <br>
        <input type="text" class="form-control" name="nombre">
    </div>
    
    <div class="form-group input-group" style="width:20%">
        <label>Fecha inicial del curso</label>
        <br>
        <input type="date" class="form-control" name="fechainicial">
    </div>
    
    <div class="form-group input-group" style="width:20%">
        <label>Fecha final del curso</label>
        <br>
        <input type="date" class="form-control" name="fechafinal">
    </div>
    
    <div class="form-group input-group" style="width:20%" >
        <label>Semestre</label>
        <br>
        <input type="text" class="form-control" name="semestre">
    </div>
    
    <div class="form-group input-group" style="width:20%" >
        <label>Limite de faltas del curso</label>
        <br>
        <input type="text" class="form-control" name="faltas">
    </div>
    
        <div class="form-group input-group" style="width:20%" >
        <label>Limite de estudiantes</label>
        <br>
        <input type="text" class="form-control" name="estudiantes">
    </div>
    
       
     <input type="submit" name="agregar_curso_uno" value="Guardar">
     <input type="submit" name="agregar_curso_otro" value="Guardar y agregar nuevo">
      	
    </form>
    <br>
    <br>