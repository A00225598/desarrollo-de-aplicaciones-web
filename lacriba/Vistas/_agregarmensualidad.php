<?php
if (file_exists("../Procesos/_util.php"))
{
    include_once("../Procesos/_util.php");
}
else
{
    include_once("Procesos/_util.php");
}
?>

    
    <form method="post" action="../index.php">
    <h2>Agregar mensualidad a curso</h2>
    <p><span class="err">(*) Todos los campos son requeridos.</span></p>
    
    <div class="form-group">
        <label>Curso*</label>
        <br>
        <select class="form-control" name="curso" style="width:20%">
            <?php mostrar_grupos(1); ?>
        </select>
    </div>              
        
    <div class="form-group input-group" style="width:20%" >
        <label>Cantidad a agregar*</label>
        <br>
        <input type="text" class="form-control" name="mensualidad">
    </div>
       
    <div class="form-group input-group" style="width:20%">
        <label>Fecha límite de pago*</label>
        <br>
        <input type="date" class="form-control" name="fecha">
    </div>
       
     <input type="submit" name="agregar_mensualidad_listo" value="Guardar">
     <input type="submit" name="agregar_mensualidad_nuevo" value="Guardar y agregar nueva">
      	
    </form>
    <br>
    <br>