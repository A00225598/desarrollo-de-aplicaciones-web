<?php
if (file_exists("../Procesos/_util.php"))
{
    include_once("../Procesos/_util.php");
}
else
{
    include_once("Procesos/_util.php");
}
?>

 
    
    <form method="post" action="../index.php">
    <h2>Agregar Usuario al sistema</h2>
    <p><span class="err">(*) Todos los campos son requeridos.</span></p>
    
     
    
    <div class="form-group input-group" style="width:20%" >
        <br>
        <label>Nombre*</label>
        <br>
        <input type="text" class= "form-control"  name="nombre" placeholder="Ejemplo : Rafael">
    </div>
    
    <div class="form-group input-group" style="width:20%" >
        <label>Direccion*</label>
        <br>
        <input type="text" class= "form-control" name="direccion" placeholder="Ejemplo : Epigmenio">
    </div>
    
    <div class="form-group input-group" style="width:20%" >
        <label>Sexo*</label>
        <br>
        <input type="text" class= "form-control" name="sexo" placeholder="Ejemplo : M/F">
     
    </div>
    
    <div class="form-group input-group" style="width:20%" >
        <label>Telefono*</label>
        <br>
        <input type="text" class= "form-control" name="telefono" placeholder="Ejemplo : 1234567">
    </div>
    
    <div class="form-group input-group" style="width:20%" >
        <label>Email*</label>
        <br>
        <input type="text" class= "form-control" name="email" placeholder="Ejemplo : lacriba@gmail.com"><br>
    </div>
    
    <div class="form-group input-group" style="width:100%" >
        <label>Rol*</label>
        <br> 
        <select class="form-control" class= "form-control" name="rol" style="width:20%">
            <?php mostrar_rol(); ?>
        </select>  
    </div> 
    
    <div class="form-group input-group" style="width:20%" >
        <label>Contraseña*</label>
        <br>
        <input type="password" class= "form-control" name="password" placeholder="Ejemplo : password"><br>
    </div>
    
     <input type="submit" name="ingresar_usuario_listo" value="Guardar">
     <input type="submit" name="agregar_usuario_mas" value="Guardar y agregar nueva">
      	
    </form>
    <br>
    <br>