<?php include_once("../Procesos/_util.php"); ?>
    
    <form method="post" action="../index.php">
    <h2>Agregar beca a alumno</h2>
    <p><span class="err">(*) Todos los campos son requeridos.</span></p>
    
    <div class="form-group input-group" style="width:20%" >
        <label>ID Alumno*</label>
        <br>
        <input type="text" class="form-control" name="ID">
    </div>
    
    <div class="form-group input-group" style="width:20%" >
        <label>Porcentaje de Beca*</label>
        <br>
        <input type="text" class="form-control" name="beca">
    </div>
    
    <div class="form-group">
        <label>Curso*</label>
        <br>
        <select class="form-control" name="curso" style="width:20%">
            <?php mostrar_grupos(1); ?>
        </select>
    </div>              
        
    <div class="form-group input-group" style="width:20%">
        <label>Fecha inicio*</label>
        <br>
        <input type="date" class="form-control" name="fechainicio">
    </div>
       
       <div class="form-group input-group" style="width:20%">
        <label>Fecha fin*</label>
        <br>
        <input type="date" class="form-control" name="fechafin">
    </div>
    
     <input type="submit" name="agregar_beca_listo" value="Guardar">
     <input type="submit" name="agregar_beca_mas" value="Guardar y agregar nueva"> 
      	
    </form>
    <br>
    <br>