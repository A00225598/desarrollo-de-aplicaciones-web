 <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                
                <a class="navbar-brand" href="index.php">La Criba</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $_SESSION["userlogged_name"]?>
                    <a href="./Procesos/_cerrarsesion.php"><i class="fa fa-fw fa-power-off"></i> Cerrar sesión</a>

                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="index.php"><i class="fa fa-fw fa-dashboard"></i>Inicio</a>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#becas"><i class="fa fa-fw fa-arrows-v"></i> Becas <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="becas" class="collapse">
                            <li>
                                <a href="_verbecasporalumno">Ver</a>
                            </li>
                            <li>
                                <a href="_agregarbeca">Agregar</a>
                            </li>
                            <li>
                                <a href="_eliminarbeca">Eliminar</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#usuarios"><i class="fa fa-fw fa-arrows-v"></i> Usuarios <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="usuarios" class="collapse">
                            <li>
                                <a href="_showuser">Ver todos</a>
                            </li>
                            <li>
                                <a href="_show_one_user">Ver un usuario</a>
                            </li>
                            <li>
                               <a href="_updateuser">Modificar</a> 
                            </li>
                            <li>
                                <a href="_adduser">Agregar</a>
                            </li>
                            <li>
                                <a href="_deleteuser">Eliminar</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#grupos"><i class="fa fa-fw fa-arrows-v"></i>Cursos<i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="grupos" class="collapse">
                            <li>
                                <a href="_vergrupos">Ver</a>
                            </li>
                            <li>
                                <a href="_agregargrupo">Agregar</a>
                            </li>
                            <li>
                                <a href="_eliminar_cursos">Eliminar</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#mensualidades"><i class="fa fa-fw fa-arrows-v"></i> Mensualidades <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="mensualidades" class="collapse">
                            <li>
                                <a href="_vermensualidadesporgrupo">Ver</a>
                            </li>
                            <li>
                                <a href="_agregarmensualidad">Agregar</a>
                            </li>
                            <li>
                                <a href="_eliminarmensualidad">Eliminar</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#pagos"><i class="fa fa-fw fa-arrows-v"></i> Pagos <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="pagos" class="collapse">
                            <li>
                                <a href="agregaPagos">Nuevo</a>
                            </li>
                            <li>
                                <a href="consultaPagos">Ver</a>
                            </li>
        
                        </ul>
                    </li>
                    <li>
                        <a><?php include_once("APIS/_translate.php"); ?> </a>
                    </li>
                    
                </ul>
                
            </div>
            <!-- /.navbar-collapse -->
        </nav>
    