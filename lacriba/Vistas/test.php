<?php include_once("./test2"); ?>

    <?php
        // define variables and set to empty values
        $id = $date = $cantidad = $tipo_de_pago = $flag = "";
        $idErr = $dateErr = $cantidadErr = $tipo_de_pagoErr = "";
        
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $flag = 0;
            if (empty($_POST["id"])) {
                $idErr = "*Se requiere un ID";
                $flag = 1;
            } else {
                $id = test_input($_POST["id"]);
                // check if id only contains number
                if (!is_numeric($id)) {
                    $idErr = "*Solo se aceptan numeros";
                    $flag = 1;
                }
            }
            
            if (empty($_POST["cantidad"])) {
                $cantidadErr = "*Se requiere una cantidad";
                $flag = 1;
            } else {
                $cantidad = test_input($_POST["cantidad"]);
                // check if amount is valid
                if (!is_numeric($cantidad) || (int)$cantidad == 0) {
                    $cantidadErr = "*Favor de agregar una cantidad a pagar";
                    $flag = 1;
                }
            }
    
            if (empty($_POST["tipo_de_pago"])) {
                $tipo_de_pagoErr = "*Se requiere un tipo de pago";
                $flag = 1;
            } else {
                $tipo_de_pago = test_input($_POST["tipo_de_pago"]);
            }
            
            if (empty($_POST["date"])) {
                $dateErr = "*Se requiere una fecha de pago";
                $flag = 1;
            } else {
                $date = test_input($_POST["date"]);
            }

        }
        
        function test_input($data) {
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
        }
    ?>
    
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
    <h2>Agregar Pagos</h2>
    <p><span class="err">*Favor de llenar todos los puntos.</span></p>
    
        <label>ID:&nbsp;</label> </label> </label> <span> <?php echo $idErr;?></span><br>
        <div class="form-group input-group" style="width:20%">
            <input type="text" class="form-control" name="id">  <br>
        </div> 
        
        <label>Fecha de pago:&nbsp;</label> <span class="err"> <?php echo $cantidadErr;?></span><br>
        <div class="form-group input-group" style="width:20%">
            <input type="date" class="form-control" name="date"> 
        </div>
        
        <label>Cantidad:&nbsp;</label> <span class="err"> <?php echo $cantidadErr;?></span><br>
        <div class="form-group input-group" style="width:20%">     
            <input type="text" class="form-control" name="cantidad"><br>
        </div>
        
        <label>Tipo de Pago:&nbsp;</label> <span class="err"> <?php echo $tipo_de_pagoErr;?></span> <br>
        <div class="form-group input-group" style="width:100%">
            <input type="radio" name="tipo_de_pago" value="E"> Efectivo &nbsp;
            <input type="radio" name="tipo_de_pago" value="C"> Tarjeta de Credito &nbsp;
            <input type="radio" name="tipo_de_pago" value="D"> Tarjeta de Debito <br>
        </div> 
        
        <input type="submit" name="agrega_pago" value="Enviar"> <br><br>
    </form>
    
    <form method="post" action="controller.php">
    <?php
        if ($_POST["submit"] == "Enviar" && $flag == 0) {
            session_start();
            echo "<hr>";
            echo "<h2>Información final:</h2>";
            echo "Nombre: "; echo $firstName; echo " "; echo $lastName; echo "<br>";
            echo "Tipo de Pago: "; echo $tipo_de_pago; echo "<br>";
            echo "Cantidad: $"; echo $cantidad; echo "<br><br>";
            
            $_SESSION["opcion"] = "agregarPago";
            $_SESSION["firstName"] = $_POST["firstName"];
            $_SESSION["lastName"] = $_POST["lastName"];
            $_SESSION["tipo_de_pago"] = $_POST["tipo_de_pago"];
            $_SESSION["cantidad"] = $_POST["cantidad"];
            echo "<p>¿Es correcto? &nbsp <input class='myButton2' type='submit' name='submit2' value='Enviar'> </p>";
        }
        
        
    ?>
    </form>