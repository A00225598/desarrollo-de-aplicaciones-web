
    <head>
        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">
    </head>
    
    <form method="post" action="./index.php">
    <h2>Consulta pagos por mes</h2>
        
        <label>Mes:&nbsp;</label> <span class="err"> <?php echo $monthErr;?></span><br>
        <div class="form-group input-group" style="width:20%">
            <select name="month" style="width:100%">
                <option value="1">Enero</option>
                <option value="2">Febrero</option>
                <option value="3">Marzo</option>
                <option value="4">Abril</option>
                <option value="5">Mayo</option>
                <option value="6">Junio</option>
                <option value="7">Julio</option>
                <option value="8">Agosto</option>
                <option value="9">Septiembre</option>
                <option value="10">Octubre</option>
                <option value="11">Noviembre</option>
                <option value="12">Diciembre</option>
            </select> 
        </div>
        
        <label>Año:&nbsp;</label> <span class="err"> <?php echo $yearErr;?></span><br>
        <div class="form-group input-group" style="width:20%">
            <input type="text" class="form-control" name="year"> 
        </div>
        
        <input type="submit" name="pagos_por_mes" value="Enviar"> <br><br>
    </form>
    