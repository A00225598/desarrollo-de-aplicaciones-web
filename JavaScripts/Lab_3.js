///////////////////////////////////////////////////
////////////TABLA
//////////////////////////////////////////////////
var size = window.prompt("Dame un numero entero: ");
var i, temp;

document.write("<strong>TABLA</strong><br><table><tr><th>Enteros</th><th>Cuadrados</th><th>Cubos</th></tr>");

for (i = 0; i < size; i++) {
        temp=i+1;
        document.write("<tr><td>" + Math.pow(temp,1) + "</td>");
        document.write("<td>"     + Math.pow(temp,2) + "</td>");
        document.write("<td>"     + Math.pow(temp,3) + "</td></tr>");
}

document.write("</table><br>");


///////////////////////////////////////////////////
////////////SUMA
//////////////////////////////////////////////////
var num1 = Math.floor((Math.random() * 100) + 1);
var num2 = Math.floor((Math.random() * 100) + 1);

var startTime = Date.now(); //Start timer
var respuesta = window.prompt("Dame el resultado de (" + num1 + " + " + num2 +") = ");
var stopTime = Date.now(); //Stop Timer

document.write("<strong>SUMA</strong><br>   " + num1 + " + " + num2 + " = " + respuesta);
if (respuesta == (num1+num2)) {
        alert("CORRECTO!!");
        document.write("  [<font color='green'>CORRECTO</font>]");     
} else {
        alert("INCORRECTO!!");
        document.write("  [<font color='red'>INCORRECTO</font>] (respuesta correcta: " + Number(num1+num2) + ")"); 
}

document.write("<br>Tiempo: " + Number((stopTime - startTime)/1000) + " seg<br>");


///////////////////////////////////////////////////
////////////Contador
//////////////////////////////////////////////////

var arr = new Array(30);
var ceros=0, negativos=0, positivos=0;

for (i=0;i < arr.length;i++){
        arr[i] = Math.floor((Math.random() * 20) - 10);        
}

for (i=0;i < arr.length;i++){
        if (arr[i]==0) {
                ceros++;          
        } else if (arr[i]>0){
                positivos++;
        } else {
                negativos++;
        }
}

document.write("<br><strong>FUNCION CONTADOR</strong><br>");
document.write("Arreglo = [");
for (i=0;i < arr.length-1;i++){
        document.write(arr[i] + ", ");       
}
document.write(arr[arr.length-1] + "]<br>");
document.write("Ceros: " + ceros + "<br>");
document.write("Negativos: " + negativos + "<br>");
document.write("Positivos: " + positivos + "<br>");


///////////////////////////////////////////////////
////////////Promedios
//////////////////////////////////////////////////
var matrix = [];
var j, average=0;
document.write("<br>");
document.write("<strong>FUNCION PROMEDIO</strong><br>");
for(i = 0; i < 10; i++){
    matrix[i] = [];
    document.write("Renglon (" + i +")  [");
    for(j = 0; j < 9; j++){ 
        matrix[i][j] = Math.floor((Math.random() * 10));
        average = matrix[i][j] + average;
        document.write(matrix[i][j] + " | ");
    }   
    matrix[i][j] = Math.floor((Math.random() * 10));
    average = matrix[i][j] + average;
    document.write(matrix[i][j] + "]  Promedio (" + Number(average/10) + ")<br>");
    average=0;
}
document.write("<br>");


///////////////////////////////////////////////////
////////////Inverso
//////////////////////////////////////////////////
var numInv = window.prompt("Dame un numero entero a invertir: ");
document.write("<strong>INVERSO</strong><br>");
document.write("Normal: " + numInv + "<br>Inverso: ");
while(numInv!=0){
       document.write(Number(numInv%10));
       numInv = (numInv - numInv%10)/10;
}
document.write("<br>");


///////////////////////////////////////////////////
////////////Temperatura
//////////////////////////////////////////////////
function newtempcel(x){
        this.cel=x;
}

function newtempfahr(x){
        this.fahr=x;
}

function celtofahr(){
    return ((this.cel*1.8) + 32);
}

function fahrtocel(){
    return ((this.fahr-32) / 1.8);
}

document.write("<br><strong>TEMPERATURA</strong><br>");
newtempcel(window.prompt("Dame temperatura en celsius: "));
document.write(this.cel + " grados Celsius es igual a " + celtofahr() + " grados Fahrenheit<br>");
newtempfahr(window.prompt("Dame temperatura en fahrenheit: "));
document.write(this.fahr + " grados Fahrenheit es igual a " + fahrtocel() + " grados Celsius");
