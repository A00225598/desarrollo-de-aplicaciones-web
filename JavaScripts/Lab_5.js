///////////////////////////////////////////////
//onClick Events for TEXT
//////////////////////////////////////////////
var textSize = 3;
var str = document.getElementById("pInfo").textContent;
document.getElementById("pInfo").innerHTML = str.fontsize(textSize);

function fontBold(){
    document.getElementById("pInfo").style.fontWeight = "900";
}

function fontItalic(){
    document.getElementById("pInfo").style.fontStyle = "italic";
}

function fontNormal(){
    document.getElementById("pInfo").removeAttribute("style");
}

function fontPlus(){
    if (textSize < 5){
        textSize ++;
    }
    document.getElementById("pInfo").innerHTML = str.fontsize(textSize);
}

function fontMinus(){
        if (textSize > 1){
        textSize--;
    }
    document.getElementById("pInfo").innerHTML = str.fontsize(textSize);
}

///////////////////////////////////////
//onMouseOver extra info Event
//////////////////////////////////////
function extraInfo() {
    var newInf = "Born in: Ciudad Obregón, Sonora.<br>" +
                 "Age: 22<br>" +
                 "Address: Privada Camino Real #114 Queretaro, Queretaro<br>";
                 
    document.getElementById("extInf").innerHTML = newInf;
}

function noExtraInfo() {
    var newInf = "";
    document.getElementById("extInf").innerHTML = newInf;
}

///////////////////////////////////////
//onMouseOver extra info Event
//////////////////////////////////////
function intervalAndTimeout(){ 
    document.getElementById("intB").innerHTML = "<font color='red'>In 5 seconds this message will disappear and a costant window will pop-up every 10 seconds.<font>";
    setInterval(function(){document.getElementById("intB").innerHTML = "";}, 5000);
}


function timeAction() {
    intervalAndTimeout();
    setInterval(function(){alert("WELCOME TO MY WEBSITE!! :D");}, 10000);
}
//////////////////////////////////
//Tic-Tac-Toe
////////////////////////////////

function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    ev.target.appendChild(document.getElementById(data));
}