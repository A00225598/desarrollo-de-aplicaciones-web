//////////////////////////////////////////////////////
// PASSWORD FUNCTION
//////////////////////////////////////////////////////
function passFunction() {
    var i, sym=1, num=1, uplet=1;
    var char;
    var pass1 = document.getElementById("usrpass1").value;
    var pass2 = document.getElementById("usrpass2").value;
    
    if (pass1 == "" || pass2 == "") {
        alert("Please fill in both spaces before verifying.");
        pass1 = "";
        pass2 = "";
        return;
    }
    
    if ((pass1 != pass2) || (pass1.length < 7)) {
        alert("Passwords do not match or are not 7 characters long. Try again.");
        pass1 = "";
        pass2 = "";
        return;
    } else {
        
        for (i=0;i<pass1.length;i++) {
            if (isNaN(pass1[i]) == false){
                num=0;
            } 
            if (/[A-Z]/.test(pass1[i])) {
                uplet=0;
            }
            if (pass1[i] == "." || pass1[i] == "!" || pass1[i] == "?" || pass1[i] == "-" || pass1[i] == "_") {
                sym=0;
            }
        }
        
        if (num+uplet+sym == 0){
            alert("The password '" + pass1 + "' is a safe password.");
        } else {
            if (num == 1) {
                alert("You are missing a number. Please try again.");
            } else if (uplet == 1) {
                alert("You are missing an upper-case letter. Please try again.");
            } else {
                alert("You are missing a symbol. Please try again.");
            }
        }
    }
}


//////////////////////////////////////////////////////
// PRODUCTOS A VENDER
//////////////////////////////////////////////////////
function saleFunction1 (){
    document.getElementById("car1sale").innerHTML = "<a><font size=5><strong>New Price: </strong> $799,999.99</font></a><br><font size=1><strong>Old Price: </strong> $900,000.00</font>";
}

function saleFunction2 (){
    document.getElementById("car2sale").innerHTML = "<a><font size=5><strong>New Price: </strong> $899,999.99</font></a><br><font size=1><strong>Old Price: </strong> $1,000,000.00</font>";
}

function saleFunction3 (){
    document.getElementById("car3sale").innerHTML = "<a><font size=5><strong>New Price: </strong> $1,199,999.99</font></a><br><font size=1><strong>Old Price: </strong> $1,300,000.00</font>";
}

function checkPrice1(){
    var x = document.getElementById("cuantos1").value;
    var tax = x*799999.99*.01;
    var tot = x*799999.99 + tax;
    var ext = Number(x) + 1;
    if (x == 3){
        document.getElementById("comment1").innerHTML = "<font color=red>Eligible for a free car!! TOTAL CARS: </font>" + ext;
    } else {
        document.getElementById("comment1").innerHTML = ""
    }
    document.getElementById("totTax1").innerHTML  = "TAX: $" + tax.toFixed(2);
    document.getElementById("totPric1").innerHTML = "TOTAL PRICE: $" + tot.toFixed(2);
}

function checkPrice2(){
    var x = document.getElementById("cuantos2").value;
    var tax = x*899999.99*.02;
    var tot = x*899999.99 + tax;
    var ext = Number(x) + 1;
    if (x == 4){
        document.getElementById("comment2").innerHTML = "<font color=red>Eligible for a free car!! TOTAL CARS: </font>" + ext;
    } else {
        document.getElementById("comment2").innerHTML = ""
    }
    document.getElementById("totTax2").innerHTML  = "TAX: $" + tax.toFixed(2);
    document.getElementById("totPric2").innerHTML = "TOTAL PRICE: $" + tot.toFixed(2);
}

function checkPrice3(){
    var x = document.getElementById("cuantos3").value;
    var tax = x*1199999.99*.1;
    var tot = x*1199999.99 + tax;
    var ext = Number(x) + 1;
    if (x == 2){
        document.getElementById("comment3").innerHTML = "<font color=red>Eligible for a free car!! TOTAL CARS: </font>" + ext;
    } else {
        document.getElementById("comment3").innerHTML = ""
    }
    document.getElementById("totTax3").innerHTML  = "TAX: $" + tax.toFixed(2);
    document.getElementById("totPric3").innerHTML = "TOTAL PRICE: $" + tot.toFixed(2);
}

//////////////////////////////////////////////////////
// PROBLEMA DE INTERES
//////////////////////////////////////////////////////


function results() {
   var c = document.getElementsByName("car");
   var m = document.getElementsByName("month");
   var I = document.getElementsByName("insurance");
   var d = document.getElementById("downpayment");
   var res = "<br>RESULTS:<br>";
   var numCar=0, numMes=0, numIns=0, tot=0, i, n1=0, n2=0, n3=0;
   document.getElementById("giveResults").innerHTML = "";
   for(i=0;i<3;i++){
      if(c[i].checked == true){
         n1 = i;
         if (i == 0){
             numCar=(799999.99*.01)+(799999.99);
         }
         if (i == 1){
             numCar=(899999.99*.02)+(899999.99);
         }
         if (i == 2){
             numCar=(1199999.99*.1)+(1199999.99);
         }
      }
      if (m[i].checked == true){
         n2 = i;
         numMes = (i + 1)*12;
      }
      if (I[i].checked == true){
         n3 = i;
         numIns=(i + 1)*1000;
      }
   }
   res += "Name: " + c[n1].value + "<br>";
   res += "Months: " + m[n2].value + "<br>";
   res += "Insurance: " + I[n3].value + "<br>";
   res += "Initial downpayment: $" + d.value + "<br>";
   
   tot=numCar+((numMes/12)*numIns);
   if (d.value > tot){
       alert("The downpayment is larger than the total cost of the car. Please try a different configuration.");
       return;
   }
   tot = (tot - d.value)/numMes;
   res += "Monthly pay (with insurance and Tax included): $" + tot.toFixed(2) + "<br>";
   
   document.getElementById("giveResults").innerHTML = res;
}