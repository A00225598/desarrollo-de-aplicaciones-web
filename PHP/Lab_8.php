<!DOCTYPE html>
<html>
   
    <head>
        <meta content="text/html; charset=UTF-8" http-equiv="content-type">
        <link rel="stylesheet" type= "text/css" href="../CSSs/Lab_8.css" />
        <h1>
            Andres Pineda Ochoa<br>
            A00225598<br>
            Laboratorio #8<br>
        </h1>
    </head>
    
    <body>
        <?php //Crea arreglos y despliegalos
            $array1 = array(10,2,1,1,4,5,9,10,0,3,7,3,2,1,2,5,6,8);
            $array2 = array(7,8,9,10,2,3,4,5,0,6,7,5,4,2,2,3,9,8,7,0,10,10,10,5,4,3,0);
        
            //Llama funciones
            lista($array1, 1);
            lista($array2, 2);
            
            echo '<form method="POST" action="Lab_8.php">
                        Dame numero entero positivo: 
                        <input type="text" name="num">
                        <input type="submit" name ="submit" value="send">
                 </form><br>';
                 
            if ($_POST["submit"] == "send") {
                  cubos($_POST["num"]);
            } 
            
            echo '<form method="POST" action="Lab_8.php">
                        Dame numero entero positivo a invertir: 
                        <input type="text" name="num2">
                        <input type="submit" name ="submit2" value="send">
                 </form><br>';
                 
            if ($_POST["submit2"] == "send") {
                  invierte($_POST["num2"]);
            } 
            
        ?>
        
        <?php //Funcion que imprime un arreglo
            function display($array) {
                echo "[";
                for ($i = 0; $i < sizeof($array); $i++) {
                    if ($i < sizeof($array) - 1) {
                        echo "$array[$i], ";
                    } else {
                        echo "$array[$i]]<br>";
                    }
                }
            }
        ?>
        
        <?php //Función que reciba un arreglo de números y devuelva su promedio
            function promediador($array) {
                $x = 0;
                
                for ($i = 0; $i < sizeof($array); $i++) {
                    $x += $array[$i];
                }
                $x = $x/sizeof($array);
                return $x;
            }
        ?>
        
        <?php //Función que reciba un arreglo de números y devuelva su mediana
            function mediana($array) {
                $middle;
                $median;
                sort($array);

                $middle = sizeof($array)/2;
                $median = $array[$middle];
                if (sizeof($array) % 2 == 0) {
                    $median = ($median + $array[$middle - 1]) / 2;
                }
                return $median;
            }
        ?>
        
        <?php //Función que reciba un arreglo de números y muestre la lista de números, 
              //y como ítems de una lista html muestre el promedio, la media, y el arreglo 
              //ordenado de menor a mayor, y posteriormente de mayor a menor.
            function lista($array, $num) {
                $lowToHigh = $highToLow = $array;
                sort($lowToHigh);
                rsort($highToLow);
                
                echo "<br><ul>
                            <li>Arreglo $num:
                                <ul>
                                    <li>";
                                        display($array);
                                    echo "</li>
                                </ul>
                            </li>
                            <li>Promedio:
                                <ul>
                                    <li>" . promediador($array) . "</li>
                                </ul>
                            </li>
                            <li>Media:
                                <ul>
                                    <li>" . mediana($array) . "</li>
                                </ul>
                            </li>
                            <li>Ordenado menor a mayor:
                                <ul>
                                    <li>";
                                        display($lowToHigh);
                              echo "</li>
                                </ul>
                            </li>
                            <li>Ordenado mayor a menor:
                                <ul>
                                    <li>";
                                        display($highToLow);
                              echo "</li>
                                </ul>
                            </li>
                        </ul><br><br>";
            }
        ?>
        
        <?php //Función que imprima una tabla html, que muestre los cuadrados y cubos de 1 hasta un número n
            function cubos($num) {
                $temp;

                echo "<table><tr><th>Enteros</th><th>Cuadrados</th><th>Cubos</th></tr>";

                for ($i = 0; $i < $num; $i++) {
                    $temp=$i+1;
                    echo "<tr><td>" . pow($temp,1) . "</td>";
                    echo "<td>"     . pow($temp,2) . "</td>";
                    echo "<td>"     . pow($temp,3) . "</td></tr>";
                }
                echo "</table><br>";
            }
        ?>
        
        <?php //Función que imprima una tabla html, que muestre los cuadrados y cubos de 1 hasta un número n
            function invierte($num) {
                
                echo "Normal: " . $num . "<br>";
                echo "Invertido: ";
                while($num != 0){
                    echo $num % 10;
                    $num = ( $num - $num % 10 ) / 10;
                }
                
                echo "<br><br>";
            }
        ?>
        
        <h1>Preguntas</h1>
        <h>¿Qué hace la función phpinfo()? Describe y discute 3 datos que llamen tu atención.</h>
        <p>The function 'phpinfo()' outputs a large amount of information about the current state of PHP. This includes information about PHP 
            compilation options and extensions, the PHP version, server information and environment (if compiled as a module), 
            the PHP environment, OS version information, paths, master and local values of configuration options, HTTP headers, 
            and the PHP License.<br><br>
            1. PHP Credits: the function displays the names of the group that made the server possible.<br>
            2. HTTP_USER_AGENT: gives out all the information about my computer and the web browser I 
               am using to call this website.<br>
            3. Calendar: I actually dont understand very well this section but I believe it's 
               possible to use the calendar to call predetermined functions such as giveDate() or giveTime() maybe.</p>
        
        <h>¿Qué cambios tendrías que hacer en la configuración del servidor para que pudiera ser apto en un ambiente de producción?</h>
        <p>A production server is a type of server that is used to deploy and host live websites or Web applications.
           It hosts websites and Web applications that have undergone extensive development and testing before they are 
           validated as production ready. In order to have a production envirorment you need three main things:<br><br>
           1. Availability: The ability for the application to be usable by its intended users during advertised hours. 
              Availability can be disrupted by any failure that affects a critical component severely enough (e.g. the 
              application crashes due to a bug, the database storage device fails, or the system administrator accidentally 
              powers off the application server). One way to promote availability is to decrease the number of single 
              points of failure in an environment.<br>
           2. Recoverability: The ability to recover an application environment in the event of system failure or data loss. 
              If a critical component fails, and is not recoverable, availability will become non-existent. Improving 
              maintainability, a related concept, reduces the time needed to perform a given recovery process in the event 
              of a failure, and therefore can improve availability in the event of a failure.<br>
           3. Performance: The application performs as expected under average or peak load (e.g. it is reasonably responsive). 
              While very important to your users, performance only matters if the application is available</p>
        <h>¿Cómo es que si el código está en un archivo con código html que se despliega del lado del cliente, se ejecuta del lado del servidor? Explica.</h>
        <p>The website/code was previously uploaded to a server, this code is then accessible by anyone who requests the website's http name. The server
           will be in charge of decrypting all of the commands, regardless of what language they are on, and will combine them all to send to the user the nice
           looking websites that we are used to seeing.</p>
        
    </body>
    
    <footer>
        
        
        
    </footer>
    
</html>