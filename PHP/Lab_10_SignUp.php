<?php
// Start the session
session_start();
?>

<!doctype html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" data-useragent="Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>DAW Lab #10</title>
    <meta name="description" content="Documentation and reference library for ZURB Foundation. JavaScript, CSS, components, grid and more."/>
    <meta name="author" content="ZURB, inc. ZURB network also includes zurb.com"/>
    <meta name="copyright" content="ZURB, inc. Copyright (c) 2015"/>
    
    <script src="../Foundation-5/js/vendor/modernizr.js"></script>
    <link rel="stylesheet" href="../Foundation-5/css/foundation.css">
</head>

<body>
    <div class="row">
    <div class="large-12 columns">
 
    </div>
        <center>
            
        </center>
        <br>
    </div>
    </div>
    <div class="row">
    <div class="large-12 columns">
        
<nav class="top-bar" data-topbar role="navigation">

  <section class="top-bar-section">
    <!-- Right Nav Section -->
    <ul class="left">
        <li>
            <a class="active" href="Lab_10_SignUp.php">Lab #10</a>
        </li>
    </ul>
  </section>
</nav>
<br>

    <?php
        // Set session variables
        $_SESSION["firstName"] = ""; $_SESSION["lastName"] = ""; $_SESSION["school"] = ""; $_SESSION["email"] = ""; $_SESSION["color"] = "";
        $_SESSION["career"] = ""; $_SESSION["age"] = ""; $_SESSION["gender"] = ""; $_SESSION["comment"] = ""; 
        $_SESSION["firstNameErr"] = ""; $_SESSION["lastNameErr"] = ""; $_SESSION["careerErr"] = ""; $_SESSION["ageErr"] = ""; 
        $_SESSION["colorErr"] = ""; $_SESSION["genderErr"] = ""; $_SESSION["errorFlag"] = "false";
        $_SESSION["im"] = ""; 
    ?>
        
    <?php
        // remove all session variables
        //session_unset(); 

        // destroy the session  
        //session_destroy(); 
    ?>
    
    <?php
        $_SESSION["errorFlag"] = "false"; //Reset flag
        
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if (empty($_POST["firstName"])) {
                $firstNameErr = "Se requiere un numbre";
                $_SESSION["errorFlag"] = "true";
            } else {
                $firstName = test_input($_POST["firstName"]);
                // check if first name only contains letters and whitespace
                if (!preg_match("/^[a-zA-Z ]*$/",$firstName)) {
                    $firstNameErr = "Nomas se aceptan letras y espacios en balnco";
                    $firstName = "";
                    $_SESSION["errorFlag"] = "true";
                }
            }
            
            if (empty($_POST["lastName"])) {
                $lastNameErr = "Se requiere un apellido";
                $_SESSION["errorFlag"] = "true";
            } else {
                $lastName = test_input($_POST["lastName"]);
                // check if last name only contains letters and whitespace
                if (!preg_match("/^[a-zA-Z ]*$/",$lastName)) {
                    $lastNameErr = "Nomas se aceptan letras y espacios en blanco";
                    $lastName = "";
                    $_SESSION["errorFlag"] = "true";
                }
            }
            
            if (empty($_POST["email"])) {
                $emailErr = "Se requiere un correo electornico";
                $_SESSION["errorFlag"] = "true";
            } else {
                $email = test_input($_POST["email"]);
                if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $emailErr = "Formato invalido"; 
                    $email = "";
                    $_SESSION["errorFlag"] = "true";
                }
            }   
            
            if (empty($_POST["color"])) {
                $colorErr = "Se requiere un color";
                $_SESSION["errorFlag"] = "true";
            } else {
                $color = test_input($_POST["color"]);
            }
            
            if (empty($_POST["career"])) {
                $careerErr = "Se requiere una carrera";
                $_SESSION["errorFlag"] = "true";
            } else {
                $career = test_input($_POST["career"]);
                if (strlen($career) > 3) {
                    $careerErr = "Carrera no valida";
                    $career = "";
                    $_SESSION["errorFlag"] = "true";
                }
            }
            
            if (empty($_POST["school"])) {
                $school= "";
            } else {
                $school = test_input($_POST["school"]);
            }
            
            if (empty($_POST["age"])) {
                $ageErr = "Se requiere una edad";
                $_SESSION["errorFlag"] = "true";
            } else {
                $age = test_input($_POST["age"]);
                // check if last name only contains numberst between 5 and 30
                if ((int)$age < 17) {
                    $ageErr = "Edad minima de 18";
                    $age = "";
                    $_SESSION["errorFlag"] = "true";
                }
            }
    
            if (empty($_POST["comment"])) {
                $comment = "";
            } else {
                $comment = test_input($_POST["comment"]);
            }
    
            if (empty($_POST["gender"])) {
                $genderErr = "Se requiere un sexo";
                $_SESSION["errorFlag"] = "true";
            } else {
                $gender = test_input($_POST["gender"]);
            }
        }
        
        function test_input($data) {
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
        }
    ?>
    
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        <form action="upload.php" method="post" enctype="multipart/form-data">
    <h2>Nueva Inscripción</h2>
    <p><span class="err">(*) Espacio requerido.</span></p>
    <div class="large-12 columns">
    <div class="row">
    <div class="large-6 columns">
        Nombre*: <span class="err"> <?php echo $firstNameErr;?></span>
        <input type="text" name="firstName">
        
        Escuela: <input type="text" name="school">
        
        Edad*: <span class="err"> <?php echo $ageErr;?></span>
        <input type="text" name="age">
        
        Color preferido*: <span class="err"> <?php echo $colorErr;?></span> <br>
        <input type="radio" name="color" value="Azul"> Azul &nbsp;&nbsp;
        <input type="radio" name="color" value="Verde"> Verde &nbsp;&nbsp;
        <input type="radio" name="color" value="Rojo"> Rojo
        <br>
    </div>
    <div class="large-6 columns">
        Apellido*: <span class="err"> <?php echo $lastNameErr;?></span>
        <input type="text" name="lastName">
        
        Carrera*: <span class="err"> <?php echo $careerErr;?></span>
        <input type="text" name="career" placeholder="Ex. ISD, IMA, ISC">
        
        Correo Electronico*: <span class="err"> <?php echo $emailErr;?></span>
        <input type="text" name="email">
        
        Sexo*: <span class="err"> <?php echo $genderErr;?></span> <br>
        <input type="radio" name="gender" value="Femenino"> Femenino &nbsp;&nbsp;
        <input type="radio" name="gender" value="Masculino"> Masculino
        <br>
    </div>
    </div>
    </div>
    
    <div class="large-12 columns">
    <div class="row">
    <div class="large-6 columns">
        Información Adicional: <textarea name="comment" rows="5" cols="20"></textarea>
    </div>
    <div class="large-6 columns">
            <br>Selecciona Imagen de Perfil:
            <input type="file" name="fileToUpload" id="fileToUpload">
            <input type="submit" value="Enviar" name="Enviar"> <br><br>
        </form>
    </div>
    </div>
    </div>
    </form>
    
    
    
    <?php if ($_POST["Enviar"] == "Enviar" && $_SESSION["errorFlag"] == "false") {echo "<hr>";}?>
    <div class="large-12 columns">
    <div class="row">
    <div class="large-6 columns"> 
        <?php
            if ($_POST["Enviar"] == "Enviar" && $_SESSION["errorFlag"] == "false") {
                echo "<h2>Información Final</h2>";
                echo "Nombre: "; echo $firstName; echo " "; echo $lastName; echo "<br>";
                echo "Edad: "; echo $age; echo "<br>";
                echo "Sexo: "; echo $gender; echo "<br>";
                echo "Carrera: "; echo $career; echo "<br>";
                echo "Escuela: "; echo $school; echo "<br>";
                echo "Correo Electronico: "; echo $email; echo "<br>";
                echo "Color Favorito: "; echo $color; echo "<br>";
                echo "Información Adicional: "; echo $comment; echo "<br>";
                echo "<br><br>";
            }
        ?>
    </div>
    <div class="large-6 columns">
        <?php 
            if ($_POST["Enviar"] == "Enviar" && $_SESSION["errorFlag"] == "false") {
                echo "<h2>Imagen de Perfil</h2>";
                echo '<img src="'.$_SESSION['imagen'].'" width="300" height="500"/><br>';
                echo "<br>";
            }
        ?>
    </div>
    
    <hr>
    <h2>Preguntas</h2>
    <h4>1. ¿Por qué es importante hacer un session_unset() y luego un session_destroy()?</h4>
    
        Primero se llama la función session_unset() para liberar la memoria de las varibles de la sesión y luego se llama la función session_destroy() para cerrar la sesión.

    <h4><br>2. ¿Cuál es la diferencia entre una variable de sesión y una cookie?</h4>
		
        Las variables por lo general son entregadas por el usuario y son guardadas en el servidor, son información importante del usuario que sera controlable y modificable por el mismo. Las cookies son variables de información que se almacenan en la computadora personal de los usarios y son creadas de manera automatica con el proposito de generar datos del usuario que le servira al dueño de la pagina proporcionar un mejor servicio, deducir tendencias del usuario, preferencias, etc.

    <h4><br>3. ¿Qué técnicas se utilizan en sitios como facebook para que el usuario no sobreescriba sus fotos en el sistema de archivos cuando sube una foto con el mismo nombre?</h4>

        Si solo se trabaja en una parte de la base de datos de la pagina web donde está situado el usuario, entonces se puede hacer un pipe (recorrido de los archivos comparando con ciertos parametros proporcionados por el programador). Es decir, si el archivo que se va a subir a la base de datos tiene un numero "9", hay que buscar si existen otras imagenes con un "9" en su nombre. Si el recorrido regresa falso entonces no hay una imagen con ese nombre. Otra tecnica seria buscar el tamaño de la imagen que se desea agregar y compararla con las imagenes previas, ya que si ninguna tiene el mismo tamaño entonces se puede deducir que la imagen es nueva.
    
    
    <footer class="row">
    <div class="large-12 columns">
    <hr/>
    <div class="row">
    <div class="large-8 columns">
        <p>Andres Pineda Ochoa A00225598</p>
    </div>
        <div class="large-4 columns">
        <p>6 de Octubre del 2015</p>
    </div>
    
    </div>
    </div>
    </footer>
    <script>
        document.write('<script src=' +
        ('__proto__' in {} ? '../Foundation-5/js/vendor/zepto' : '../Foundation-5/js/vendor/jquery') +
        '.js><\/script>')
    </script>
    <script src="../Foundation-5/js/vendor/modernizr.js"></script>
    <script src="../Foundation-5/js/vendor/jquery.js"></script>
    <script src=",,.Foundation-5/js/foundation.min.js"></script>
    <script src="../Foundation-5/js/foundation/foundation.js"></script>
    <script src="../Foundation-5/js/foundation/foundation.clearing.js"></script>
    <script>
        $(document).foundation();
    </script>
    <script src="../assets/js/templates/jquery.js"></script>
    <script src="../assets/js/templates/foundation.js"></script>
    <script>
        $(document).foundation();

        var doc = document.documentElement;
        doc.setAttribute('data-useragent', navigator.userAgent);
    </script>
</body>
</html>