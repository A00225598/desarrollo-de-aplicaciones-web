<!doctype html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" data-useragent="Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>DAW Lab #9</title>
    <meta name="description" content="Documentation and reference library for ZURB Foundation. JavaScript, CSS, components, grid and more."/>
    <meta name="author" content="ZURB, inc. ZURB network also includes zurb.com"/>
    <meta name="copyright" content="ZURB, inc. Copyright (c) 2015"/>
    
    <script src="../Foundation-5/js/vendor/modernizr.js"></script>
    <link rel="stylesheet" href="../Foundation-5/css/foundation.css">
</head>

<body>
    <div class="row">
    <div class="large-12 columns">
 
    </div>
        <center>
            
        </center>
        <br>
    </div>
    </div>
    <div class="row">
    <div class="large-12 columns">
        
<nav class="top-bar" data-topbar role="navigation">

  <section class="top-bar-section">
    <!-- Right Nav Section -->
    <ul class="left">
        <li>
            <a class="active" href="Lab_9.php">Lab #9</a>
        </li>
    </ul>
  </section>
</nav>
<br>
        

    
    <?php
        // define variables and set to empty values
        $firstName = $lastName = $studentID = $address = $postalCode = $email = $phone = $day = $career = $city = $age = $gender = $comment  = "";
        $firstNameErr = $lastNameErr = $studentIDErr = $careerErr = $phoneErr = $ageErr = $dayErr = $genderErr = "";
        
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if (empty($_POST["firstName"])) {
                $firstNameErr = "First name is required";
            } else {
                $firstName = test_input($_POST["firstName"]);
                // check if first name only contains letters and whitespace
                if (!preg_match("/^[a-zA-Z ]*$/",$firstName)) {
                    $firstNameErr = "Only letters and white spaces are allowed";
                    $firstName = "";
                }
            }
            
            if (empty($_POST["lastName"])) {
                $lastNameErr = "Last name is required";
            } else {
                $lastName = test_input($_POST["lastName"]);
                // check if last name only contains letters and whitespace
                if (!preg_match("/^[a-zA-Z ]*$/",$lastName)) {
                    $lastNameErr = "Only letters and white spaces are allowed";
                    $lastName = "";
                }
            }
            
            if (empty($_POST["studentID"])) {
                $studentIDErr = "Student ID is required";
            } else {
                $studentID = test_input($_POST["studentID"]);
                if ($studentID[0] != 'A' || strlen($studentID) != 9 ) {
                    $studentIDErr = "Invalid format, try again.";
                    $studentID = "";
                }
            }
            
            if (empty($_POST["address"])) {
                $address = "";
            } else {
                $address = test_input($_POST["address"]);
            }
            
            if (empty($_POST["postalCode"])) {
                $postalCode = "";
            } else {
                $postalCode = test_input($_POST["postalCode"]);
            }
            
            if (empty($_POST["email"])) {
                $emailErr = "An Email is required";
            } else {
                $email = test_input($_POST["email"]);
                if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $emailErr = "Invalid email format"; 
                    $email = "";
                }
            }   
            
            if (empty($_POST["phone"])) {
                $phoneErr = "Phone number is required";
            } else {
                $phone = test_input($_POST["phone"]);
            }
            
            if (empty($_POST["day"])) {
                $dayErr = "A day is required";
            } else {
                $day = test_input($_POST["day"]);
            }
            
            if (empty($_POST["career"])) {
                $careerErr = "Career is required";
            } else {
                $career = test_input($_POST["career"]);
                if (strlen($career) > 3) {
                    $careerErr = "Invalid career ID";
                    $career = "";
                }
            }
            
            if (empty($_POST["city"])) {
                $city = "";
            } else {
                $city = test_input($_POST["city"]);
            }
            
            if (empty($_POST["age"])) {
                $ageErr = "An age is required";
            } else {
                $age = test_input($_POST["age"]);
                // check if last name only contains numberst between 5 and 30
                if (((int)$age < 5) || ((int)$age > 30)) {
                    $ageErr = "Ages between 5 and 30 only";
                    $age = "";
                }
            }
    
            if (empty($_POST["comment"])) {
                $comment = "";
            } else {
                $comment = test_input($_POST["comment"]);
            }
    
            if (empty($_POST["gender"])) {
                $genderErr = "Gender is required";
            } else {
                $gender = test_input($_POST["gender"]);
            }
        }
        
        function test_input($data) {
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
        }
    ?>
    
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
    <h2>School Field Trip Registration Form</h2>
    <p><span class="err">(*) Required field.</span></p>
    <div class="large-12 columns">
    <div class="row">
    <div class="large-6 columns">
        First Name*: <span class="err"> <?php echo $firstNameErr;?></span>
        <input type="text" name="firstName">
        
        Student ID*: <span class="err"> <?php echo $studentIDErr;?></span>
        <input type="text" name="studentID" placeholder="Ex. A00225598">
        
        Home Address: <input type="text" name="address">
        Postal Code: <input type="text" name="postalCode">
        
        Phone*: <span class="err"> <?php echo $phoneErr;?></span>
        <input type="text" name="phone" placeholder="Ex. (644) 156-0687">
        
        Day of preference*: <span class="err"> <?php echo $dayErr;?></span> <br>
        <input type="radio" name="day" value="Monday"> Monday &nbsp;&nbsp;
        <input type="radio" name="day" value="Tuesday"> Tuesday &nbsp;&nbsp;
        <input type="radio" name="day" value="Wednesday"> Wednesday
        <br>
    </div>
    <div class="large-6 columns">
        Last Name*: <span class="err"> <?php echo $lastNameErr;?></span>
        <input type="text" name="lastName">
        
        Career*: <span class="err"> <?php echo $careerErr;?></span>
        <input type="text" name="career" placeholder="Ex. ISD, IMA, ISC">
        
        City: <input type="text" name="city">
        
        Email*: <span class="err"> <?php echo $emailErr;?></span>
        <input type="text" name="email">
        
        Age*: <span class="err"> <?php echo $ageErr;?></span>
        <input type="text" name="age">
        
        Gender*: <span class="err"> <?php echo $genderErr;?></span> <br>
        <input type="radio" name="gender" value="Female"> Female &nbsp;&nbsp;
        <input type="radio" name="gender" value="Male"> Male
        <br>
    </div>
    </div>
    </div>
    
    <div class="large-12 columns">
    <div class="row">
    <div class="large-7 columns">
        Comments: <textarea name="comment" rows="5" cols="20"></textarea>
        <input type="submit" name="submit" value="Submit"> <br><br>
    </div>
    </div>
    </div>
    </form>

    <?php
        if ($_POST["submit"] == "Submit") {
            echo "<hr>";
            echo "<h2>Your Final Information:</h2>";
            echo "Name: "; echo $firstName; echo " "; echo $lastName; echo "<br>";
            echo "Age: "; echo $age; echo "<br>";
            echo "Gender: "; echo $gender; echo "<br>";
            echo "Student ID: "; echo $studentID; echo "<br>";
            echo "Career: "; echo $career; echo "<br>";
            echo "Address: "; echo $address; echo "<br>";
            echo "City: "; echo $city; echo "<br>";
            echo "Postal code: "; echo $postalCode; echo "<br>";
            echo "Phone number: "; echo $phone; echo "<br>";
            echo "Email: "; echo $email; echo "<br>";
            echo "Day chosen: "; echo $day; echo "<br>";
            echo "Comment: "; echo $comment; echo "<br>";
        }
    ?>
    
    <hr>
    <h2>Preguntas</h2>
    <h4>1. ¿Por qué es una buena práctica separar el controlador de la vista?</h4>
    
        Es importante mantener un orden dentro de tu código ya que el tener diferentes lenguajes de programación puede generar confusion. Se recomienda tener por ejemplo al inicio del script la sección de php (variables, funciones, tec.), después el código html y al final las funciones de javascript. Esto le da facilidad al programador de modificar y entender el código. 

    <h4><br>2. Aparte de los arreglos $_POST y $_GET, ¿qué otros arreglos están predefinidos en php y cuál es su función?</h4>
		
        <strong>- $_POST:</strong> matriz que maneja variables obtenidas por el método HTTP 	POST. Generalmente enviadas por un formulario. Ej: El form tiene un input con id/name igual a Nombre, nosotros le ponemos el valor Pablo, luego hacemos print $_POST[‘nombre’], y obtendríamos Pablo. 
    <br><strong>- $_GET:</strong> matriz que maneja variables obtenidas mediante parámetros URL. Ej: Tenemos www.agevaled.com.ar?nombre=Pablo, ahí obtenemos si hacemos print $_GET[‘nombre’], obtendríamos Pablo.
    <br><strong>- $_COOKIE:</strong> matriz que maneja variables obtenidas mediante HTTP Cookies. Ej: Se tiene que inicializar una variable cookies antes, por ejemplo setcookie(“nombre”, “Pablo”); después si hacemos print $_COOKIE[‘nombre’], obtendríamos Pablo.
    <br><strong>- $_REQUEST:</strong> matriz que maneja variables variables relacionadas con todo lo que contiene $_GET, $_POST y $_COOKIE.

    <h4><br>3. Explora las funciones de php, y describe 2 que no hayas visto en otro lenguaje y que llamen tu atención.</h4>

        1. <strong>imagedestroy():</strong> frees any memory associated with image image.
    <br>2. <strong>phpinfo():</strong> Outputs a large amount of information about the current state of PHP. This includes information about PHP compilation options and extensions, the PHP version, server information and environment (if compiled as a module), the PHP environment, OS version information, paths, master and local values of configuration options, HTTP headers, and the PHP License.
    <br>3. <strong>function_exists(function_name):</strong> Returns true if the given function has been defined.
    <br>
    
    
    <footer class="row">
    <div class="large-12 columns">
    <hr/>
    <div class="row">
    <div class="large-8 columns">
        <p>Andres Pineda Ochoa A00225598</p>
    </div>
        <div class="large-4 columns">
        <p>25 de Septiembre del 2015</p>
    </div>
    
    </div>
    </div>
    </footer>
    <script>
        document.write('<script src=' +
        ('__proto__' in {} ? '../Foundation-5/js/vendor/zepto' : '../Foundation-5/js/vendor/jquery') +
        '.js><\/script>')
    </script>
    <script src="../Foundation-5/js/vendor/modernizr.js"></script>
    <script src="../Foundation-5/js/vendor/jquery.js"></script>
    <script src=",,.Foundation-5/js/foundation.min.js"></script>
    <script src="../Foundation-5/js/foundation/foundation.js"></script>
    <script src="../Foundation-5/js/foundation/foundation.clearing.js"></script>
    <script>
        $(document).foundation();
    </script>
    <script src="../assets/js/templates/jquery.js"></script>
    <script src="../assets/js/templates/foundation.js"></script>
    <script>
        $(document).foundation();

        var doc = document.documentElement;
        doc.setAttribute('data-useragent', navigator.userAgent);
    </script>
</body>
</html>