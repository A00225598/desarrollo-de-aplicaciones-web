<!doctype html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" data-useragent="Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>DAW Lab #11</title>
    <meta name="description" content="Documentation and reference library for ZURB Foundation. JavaScript, CSS, components, grid and more."/>
    <meta name="author" content="ZURB, inc. ZURB network also includes zurb.com"/>
    <meta name="copyright" content="ZURB, inc. Copyright (c) 2015"/>
    
    <script src="../Foundation-5/js/vendor/modernizr.js"></script>
    <link rel="stylesheet" href="../Foundation-5/css/foundation.css">
</head>

<body>
    <div class="row">
    <div class="large-12 columns">
 
    </div>
        <center>
            
        </center>
        <br>
    </div>
    </div>
    <div class="row">
    <div class="large-12 columns">
        
<nav class="top-bar" data-topbar role="navigation">

  <section class="top-bar-section">
    <!-- Right Nav Section -->
    <ul class="left">
        <li>
            <a class="active" href="preguntas.php">Preguntas</a>
        </li>
        <li>
            <a class="active" href="agregaPagos.php">Agregar Pago</a>
        </li>
        <li>
            <a class="active" href="consultaPagos.php">Consultar Pagos</a>
        </li>
        <li>
            <a class="active" href="consultaDeudores.php">Consultar Deudores</a>
        </li>
    </ul>
  </section>
</nav>
<br>
        

    
    <?php
        // define variables and set to empty values
        $firstName = $lastName = $cantidad = $tipo_de_pago = $flag = "";
        $firstNameErr = $lastNameErr = $cantidadErr = $tipo_de_pagoErr = "";
        
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $flag = 0;
            if (empty($_POST["firstName"])) {
                $firstNameErr = "Se requiere un nombre";
                $flag = 1;
            } else {
                $firstName = test_input($_POST["firstName"]);
                // check if first name only contains letters and whitespace
                if (!preg_match("/^[a-zA-Z ]*$/",$firstName)) {
                    $firstNameErr = "Solo se aceptan letras y espacios en blanco";
                    $firstName = "";
                    $flag = 1;
                }
            }
            
            if (empty($_POST["lastName"])) {
                $lastNameErr = "Se requiere un apellido";
                $flag = 1;
            } else {
                $lastName = test_input($_POST["lastName"]);
                // check if last name only contains letters and whitespace
                if (!preg_match("/^[a-zA-Z ]*$/",$lastName)) {
                    $lastNameErr = "Solo se aceptan letras y espacios en blanco";
                    $lastName = "";
                    $flag = 1;
                }
            }
            
            if (empty($_POST["cantidad"])) {
                $cantidadErr = "Se requiere una cantidad";
                $flag = 1;
            } else {
                $cantidad = test_input($_POST["cantidad"]);
                // check if amount is valid
                if (!is_numeric($cantidad) || (int)$cantidad == 0) {
                    $cantidadErr = "Favor de agregar una cantidad a pagar";
                    $cantidad = "";
                    $flag = 1;
                }
            }
    
            if (empty($_POST["tipo_de_pago"])) {
                $tipo_de_pagoErr = "Se requiere un tipo de pago";
                $flag = 1;
            } else {
                $tipo_de_pago = test_input($_POST["tipo_de_pago"]);
            }

        }
        
        function test_input($data) {
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
        }
    ?>
    
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
    <h2>Agregar Pagos</h2>
    <p><span class="err">*Favor de llenar todos los puntos.</span></p>
    <div class="large-12 columns">
    <div class="row">
    <div class="large-6 columns">
        Nombre: <span class="err"> <?php echo $firstNameErr;?></span>
        <input type="text" name="firstName">
        
        Tipo de Pago: <span class="err"> <?php echo $tipo_de_pagoErr;?></span> <br>
        <input type="radio" name="tipo_de_pago" value="E"> Efectivo &nbsp;
        <input type="radio" name="tipo_de_pago" value="C"> Tarjeta de Credito &nbsp;
        <input type="radio" name="tipo_de_pago" value="D"> Tarjeta de Debito <br>
                
        Cantidad: <span class="err"> <?php echo $cantidadErr;?></span>
        <input type="text" name="cantidad">
        
        <input class="myButton" type="submit" name="submit" value="Enviar"> <br><br>

    </div>
    <div class="large-6 columns">
        Apellido: <span class="err"> <?php echo $lastNameErr;?></span>
        <input type="text" name="lastName">
        
    </div>
    </div>
    </div>
    </form>
    
    <form method="post" action="controller.php">
    <?php
        if ($_POST["submit"] == "Enviar" && $flag == 0) {
            session_start();
            echo "<hr>";
            echo "<h2>Información final:</h2>";
            echo "Nombre: "; echo $firstName; echo " "; echo $lastName; echo "<br>";
            echo "Tipo de Pago: "; echo $tipo_de_pago; echo "<br>";
            echo "Cantidad: $"; echo $cantidad; echo "<br><br>";
            
            $_SESSION["opcion"] = "agregarPago";
            $_SESSION["firstName"] = $_POST["firstName"];
            $_SESSION["lastName"] = $_POST["lastName"];
            $_SESSION["tipo_de_pago"] = $_POST["tipo_de_pago"];
            $_SESSION["cantidad"] = $_POST["cantidad"];
            echo "<p>¿Es correcto? &nbsp <input class='myButton2' type='submit' name='submit2' value='Enviar'> </p>";
        }
    ?>
    </form>
    
    
    <footer class="row">
    <div class="large-12 columns">
    <hr/>
    <div class="row">
    <div class="large-8 columns">
        <p>Andres Pineda Ochoa A00225598</p>
    </div>
        <div class="large-4 columns">
        <p>
            <?php 
                $today=getdate(date("U"));
                echo $today[month]." ".$today[mday].", ".$today[year];
            ?>
        </p>
    </div>
    
    </div>
    </div>
    </footer>
    <script>
        document.write('<script src=' +
        ('__proto__' in {} ? '../Foundation-5/js/vendor/zepto' : '../Foundation-5/js/vendor/jquery') +
        '.js><\/script>')
    </script>
    <script src="../Foundation-5/js/vendor/modernizr.js"></script>
    <script src="../Foundation-5/js/vendor/jquery.js"></script>
    <script src=",,.Foundation-5/js/foundation.min.js"></script>
    <script src="../Foundation-5/js/foundation/foundation.js"></script>
    <script src="../Foundation-5/js/foundation/foundation.clearing.js"></script>
    <script>
        $(document).foundation();
    </script>
    <script src="../assets/js/templates/jquery.js"></script>
    <script src="../assets/js/templates/foundation.js"></script>
    <script>
        $(document).foundation();

        var doc = document.documentElement;
        doc.setAttribute('data-useragent', navigator.userAgent);
    </script>
</body>
</html>