<!doctype html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" data-useragent="Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)">
<head>
    <link rel="stylesheet" href="../Foundation-5/css/foundation.css">
</head>

<?php
// Start the session
session_start();
include_once("util.php");

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
if ($_SESSION["opcion"] == "agregarPago") {
    
    //$_SESSION["firstName"];
    //$_SESSION["lastName"];
    $result = nuevoPago($_SESSION["cantidad"], $_SESSION["tipo_de_pago"]);
    if ($result == true) echo "<br>Nuevo pago ejecutado exitosamente.";
    else echo "<br>Error al crear pago. Intente de nuevo.";
    mysql_free_result($result);

}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
if ($_SESSION["opcion"] == "consultaDeudores") {
    $result = consultaDeudores($_SESSION["mes"]);
    
    //Table
    echo "<table cellspacing='0'>";
	//Table Header
	echo "<thead>";
		echo "<tr>";
			echo "<th>ID</th>";
			echo "<th>Fecha</th>";
			echo "<th>Cantidad</th>";
			echo "<th>Tipo de Pago</th>";
			echo "<th>ID Usuario</th>";
		echo "</tr>";
	echo "</thead>";
	//Table Header

	//Table Body
	echo "<tbody>";
    
    while ($row = mysqli_fetch_assoc($result)) {
        //Table Row
        echo "<tr>";
            echo "<td>" . $row['ID'] .           "</td>";
            echo "<td>" . $row['Fecha'] .        "</td>";
            echo "<td>" . $row['Cantidad'] .     "</td>";
            $temp = $row['Tipo_de_pago'];
            if ($temp == "E") echo "<td>Efectivo</td>";
            if ($temp == "C") echo "<td>Credito</td>";
            if ($temp == "D") echo "<td>Debito</td>";
            echo "<td>" . $row['ID_usuario'] .   "</td>";
        echo "</tr>";
		//Table Row
    }
    
    	echo "</tbody>";
	    //Table Body
    echo "</table>";
    //End Table
    
    mysql_free_result($result);
}


// remove all session variables
session_unset(); 

// destroy the session  
session_destroy(); 

?>

</html>